import { LitElement, html } from 'lit-element';

class PersonaSidebar extends LitElement {

    static get properties(){
        return{
            peopleStats: {type: Object},
            destinos: {type: Array}
        };
    }

    constructor(){
        super();
        this.peopleStats = {};
        this.destinos = [];
        this.getDestinosData();
    }
//<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"></link>

    //https://bootstrap.themes.guide/hootstrap/theme.min.css
    render(){
       return html`
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"></link>
        <aside>
        <section>
            <br />                  
            <br />    
            <div class="mt-5">
                <button class="w-100 btn btn-outline-light" style="font-size: 50px"
                @click="${this.newPerson}"><strong>Alta</strong>
                <img src="./img/button.png" width="50" />
                </button>
            </div>
            <br />
            <br />
            <br />
            <div class="form-group">                                    
            <label for="destino" class="bg btn-outline-light">Filtra por un destino </label>
            <br />
            <br />
            <select  name="destino" id="destino"  @input="${this.getFilter}">
                <optgroup>
                    <option selected="selected" value="Todos""> -- Todos -- </option>
                    ${this.destinos.map(
                        destino =>  html`
                        <option .value="${destino.destiny}">${destino.destiny}</option>`
                    )}
                </optgroup>
            </select>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div class="bg btn-outline-light">
            
                Hay <strong> ${this.peopleStats.numberOfPeople}</strong> alumnos apuntados
            </div>
        </section>
    </aside>
       `; 
    }


    newPerson(){
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear una nueva persona");

        this.dispatchEvent(new CustomEvent("new-person",{}));
    }

    getFilter(e){
        console.log("getFilter en persona-sidebar");
        console.log("Se va a filtrar por destino " + e.target.value);

        this.dispatchEvent(new CustomEvent("filter-destino",{
            "detail": {
                filterDestiny: e.target.value
                }
            }
        ));
    }


    getDestinosData(){
        console.log("getDestinosData");
        console.log("Obteniendo datos de los destinos");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            // Estamos contemplando el 200, pero estoy hay que consesuarlo con el BACK
            if (xhr.status === 200){
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                // Esto va a cambiar en Hackathon
                this.destinos = APIResponse;
            }
        };
        // Aquí pondremos la url que nos digan los del BACK
        xhr.open("GET", "http://localhost:8081/apitechu/v3/travells");
        xhr.send();
        console.log("Fin de getDestinosData");

    }

}

customElements.define('persona-sidebar', PersonaSidebar)