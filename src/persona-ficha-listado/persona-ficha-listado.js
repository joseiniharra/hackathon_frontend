import { LitElement, html } from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties(){
        return{
            dni: {type: String},
            name: {type: String},
            course: {type: String},
            phone: {type: Number},
            email: {type: String},
            destiny: {type: String}
        };
    }

    constructor(){
        super();
    }

    //<img src="${this.photo.src}" height="250" width="250" alt="${this.photo.alt}">    
/*
    <ul class="list-group list-group-flush" onload="${this.translateDestination}">
    <li class="list-group-item">Destino elegido ${this.nameDestination}</li>
    </ul>
*/
    render(){
       return html`
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
       <div class="card h-100">   

            <div class="card-body">
                <table class="table table-sm ">
                    <thead>
                        <tr>
                            <th scope="col align-top"> <h5 class="card-text">Nombre</h5></th>
                            <th scope="col"> <h5 class="card-text"><strong>${this.name}</strong></h5></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="col"> <h5 class="card-text">Destino</h4></th>
                                <th scope="col"> <h5 class="card-text"><strong>${this.destiny}</strong></h4></th>
                            </tr>
                            <tr>
                                <th scope="col"> <h6 class="card-text">DNI</h6></th>
                                <th scope="col"> <h6 class="card-text"><strong>${this.dni}</strong></h6></th>
                            </tr>
                            <tr>
                                <th scope="col"> <h6 class="card-text">Curso</h6></th>
                                <th scope="col"> <h6 class="card-text"><strong>${this.course}</strong></h6></th>
                            </tr>
                            <tr>
                                <th scope="col"> <h6 class="card-text">Teléfono</h6></th>
                                <th scope="col"> <h6 class="card-text"><strong>${this.phone}</strong></h6></th>
                            </tr>
                            <tr>
                                <th scope="col"> <h6 class="card-text">e-mail</h6></th>
                                <th scope="col"> <h6 class="card-text"><strong>${this.email}</strong></h6></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer btn-group mr-2" role="group" aria-label="First group">
                    <button @click="${this.deletePerson}" class="btn btn-secondary col-5"><strong>Borrar</strong></button>
                    <button @click="${this.moreInfo}" class="btn btn-success col-5 offset-1"><strong>Editar</strong></button>
                </div>
            </div>
       `; 
    }

    deletePerson(e){
        console.log("deletePerson");
        console.log("Se va a borrar la persona con dni " + this.dni);

        this.dispatchEvent(
            new CustomEvent("delete-person", {
                detail: {
                    "dni" : this.dni
                }
            })
        );
    }

    moreInfo(e){
        console.log("moreInfo");
        console.log("Se ha pedido más información de la persona " + this.name + "con dni " + this.dni);
        
        this.dispatchEvent(
            new CustomEvent("info-person", {
                detail: {
                    "dni" : this.dni
                }
            })
        );
      
    }
 /*   
    translateDestination() {
        console.log ("translateDestination");

        if (this.destiny===1) {
            this.nameDestination = "Playa"
        } else {
            this.nameDestination = "Montaña"
        };
    }
*/
}

customElements.define('persona-ficha-listado', PersonaFichaListado)